#!/bin/bash

echo "Change the permission of the volume"
IFS=',' read -ra VOLUME <<< "$VOLUME_LIST"
for i in "${VOLUME[@]}"; do
	chown -R jboss:root "$i"
	chmod 755 "$i"
done