#!/bin/bash

cp /opt/jboss/appl/setup/ssl/server-keystore.jks /opt/jboss/wildfly/standalone/configuration/

#Setup Admin Console
sh /opt/jboss/wildfly/bin/add-user.sh hsadmin hsadmin#1 --silent

#Setup JBOSS Container by executing JBOSS CLI
$APP_SETUP_HOME/scripts/execute-jboss-cli.sh

rm -r /opt/jboss/wildfly/standalone/log/** && echo "All logs are cleaned up succesfully"