#!/bin/bash

if [ ! $ENABLE_NEW_RELIC ]; then
   echo "New Relic isn't enabled"
   exit 0	
fi

NEW_RELIC_HOME=${NEW_RELIC_HOME:-"$APP_SETUP_HOME/newrelic"}
NEW_RELIC_AGENT_VERSION=${NEW_RELIC_AGENT_VERSION:-"4.11.0"}
NEW_RELIC_AGENT_JAR="newrelic-agent-$NEW_RELIC_AGENT_VERSION.jar"
cd ${NEW_RELIC_HOME} && curl -O https://download.newrelic.com/newrelic/java-agent/newrelic-agent/$NEW_RELIC_AGENT_VERSION/$NEW_RELIC_AGENT_JAR
# Validate if this is real jar file. If it is jar file, unzip -l will succeed.
unzip -l $NEW_RELIC_HOME/$NEW_RELIC_AGENT_JAR
result=$?
if [ "${result}" -eq "0" ]; then
	JAVA_OPTS="$JAVA_OPTS -javaagent:$NEW_RELIC_HOME/$NEW_RELIC_AGENT_JAR"
	JAVA_OPTS="$JAVA_OPTS -Dnewrelic.config.file=$NEW_RELIC_HOME/newrelic.yml"
	JAVA_OPTS="$JAVA_OPTS -Dnewrelic.config.process_host.display_name=\"HSSOMCreditCardInfoApp\""
	JAVA_OPTS="$JAVA_OPTS -Dnewrelic.logfile=/opt/jboss/wildfly/standalone/log/newrelic_HSSOMCreditCardInfoApp.log"
	JAVA_OPTS="$JAVA_OPTS -Dnewrelic.config.distributed_tracing.enabled=true"
	echo $JAVA_OPTS >> $APP_SETUP_HOME/java-opts.txt
fi