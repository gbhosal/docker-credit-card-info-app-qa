#!/bin/bash

XMS=${XMS:-"128m"}
XMX=${XMX:-"256m"}
METASPACE=${METASPACE:-"128m"}
MAX_METASPACE_SIZE=${MAX_METASPACE_SIZE:-"512m"}
JAVA_OPTS="-Xms$XMS -Xmx$XMX -Dsun.rmi.dgc.client.gcInterval=900000 -Dsun.rmi.dgc.server.gcInterval=900000 -Djava.io.tmpdir=/tmp -Ddata.directory=/opt/jboss/appl/data/"
JAVA_OPTS="$JAVA_OPTS -Dlog.logdir=/opt/jboss/wildfly/standalone/log/ -Duser.dir=/opt/jboss/appl/data/ -Dit.frech.collector.port=59400"
JAVA_OPTS="$JAVA_OPTS -Dcom.ibm.msg.client.commonservices.log.outputName=/opt/jboss/wildfly/standalone/log -Dcom.ibm.msg.client.commonservices.trace.outputName=/opt/jboss/wildfly/standalone/log"
JAVA_OPTS="$JAVA_OPTS -XX:MetaspaceSize=$METASPACE -XX:MaxMetaspaceSize=$MAX_METASPACE_SIZE"
JAVA_OPTS="$JAVA_OPTS -XX:+UseConcMarkSweepGC"
JAVA_OPTS="$JAVA_OPTS -Xloggc:/opt/jboss/wildfly/standalone/log/gc.$(date "+%Y%m%d-%H%M%S").log"
JAVA_OPTS="$JAVA_OPTS -XX:+PrintGCDetails -XX:+PrintGCTimeStamps"
JAVA_OPTS="$JAVA_OPTS -Djboss.remoting.pooled-buffers=false"
JAVA_OPTS="$JAVA_OPTS -DLog4jContextSelector=org.apache.logging.log4j.core.async.AsyncLoggerContextSelector"
JAVA_OPTS="$JAVA_OPTS -Dlogback.configurationFile=/opt/jboss/appl/data/log4j.properties"
JAVA_OPTS="$JAVA_OPTS -Djava.net.preferIPv4Stack=true -Djava.net.preferIPv4Addresses" 
echo $JAVA_OPTS >> $APP_SETUP_HOME/java-opts.txt