#!/bin/bash

JBOSS_HOME=/opt/jboss/wildfly
JBOSS_CLI=$JBOSS_HOME/bin/jboss-cli.sh
JBOSS_MODE=${1:-"standalone"}
JBOSS_CONFIG=${2:-"$JBOSS_MODE.xml"}

function wait_for_server() {
  echo "=> Starting WildFly server"
  $JBOSS_HOME/bin/$JBOSS_MODE.sh -c $JBOSS_CONFIG > /dev/null &
  
  echo "=> Waiting for the server to boot"
  until `$JBOSS_CLI -c "ls /deployment" &> /dev/null`; do
    sleep 1
  done
}

function stop_server() {
  echo "=> Shutting down WildFly"
  if [ "$JBOSS_MODE" = "standalone" ]; then
     $JBOSS_CLI -c ":shutdown"
  else
     $JBOSS_CLI -c "/host=*:shutdown"
  fi
  sleep 10s
}

wait_for_server

echo "=> Setting up jdbc-drivers"
$JBOSS_CLI -c --file=$APP_SETUP_HOME/jboss-cli/jdbc-drivers.cli

echo "=> Setting up security domains"
$JBOSS_CLI -c --file=$APP_SETUP_HOME/jboss-cli/security-domain.cli

echo "=> Setting up datasources"
$JBOSS_CLI -c --file=$APP_SETUP_HOME/jboss-cli/datasources.cli

echo "=> Setting up SSL"
$JBOSS_CLI -c --file=$APP_SETUP_HOME/jboss-cli/ssl-setup.cli

echo "=> Reloading standalone.xml configuration"
stop_server

rm -r /opt/jboss/wildfly/standalone/configuration/standalone_xml_history

wait_for_server
stop_server