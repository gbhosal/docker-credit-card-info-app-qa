#!/bin/bash

$APP_SETUP_HOME/scripts/setup-env-vars.sh && echo "Setup JAVA_OPTS - Step 1 completed successfully"
$APP_SETUP_HOME/scripts/install-newrelic.sh && echo "Setup JAVA_OPTS (New Relic) - Step 2 completed successfully"
export JAVA_OPTS=$(cat $APP_SETUP_HOME/java-opts.txt)
echo "JAVA_OPTS=$JAVA_OPTS"
/opt/jboss/wildfly/bin/standalone.sh -b 0.0.0.0 -bmanagement 0.0.0.0