FROM jboss/wildfly:10.1.0.Final
USER root
RUN yum update -y
RUN usermod -u 918 jboss & groupmod -g 918 jboss & chown -R jboss:0 /opt/jboss/wildfly
ENV TZ=America/New_York APP_SETUP_HOME=/opt/jboss/appl/setup APP_DATA=/opt/jboss/appl/data VOLUME_LIST="/opt/jboss/wildfly/standalone/log"
COPY --chown=jboss:0 setup $APP_SETUP_HOME
COPY --chown=jboss:0 data $APP_DATA
RUN chmod -R 755 $APP_SETUP_HOME/scripts
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
USER jboss
RUN sh $APP_SETUP_HOME/scripts/setup-server.sh
VOLUME ["/opt/jboss/wildfly/standalone/log"]
USER root
RUN sh $APP_SETUP_HOME/scripts/volume-permission.sh
USER jboss
ENTRYPOINT ["/bin/bash", "/opt/jboss/appl/setup/scripts/start-server.sh"]